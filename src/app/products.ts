﻿export const dummyData = [
  {
    name: 'Phase 1',
    revision: 151,
    auditType: "Test Audit",
    ecnId: 876484876956959,
    processStatus: {value: 3},
    dateTimeCreated: '2017-04-12 11:32:54.765',
    phase: {phsNo: 0},
    auditLog: [
      {"Narrative": "TestN1", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN2", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Warning"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN4", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Fatal"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"}
    ]
  },
  {
    name: 'Phase 2',
    revision: 151,
    auditType: "Test Audit",
    ecnId: 876484876956959,
    processStatus: {value: 2},
    dateTimeCreated: '2017-04-12 11:32:54.800',
    phase: {phsNo: 1},
    auditLog: [
      {"Narrative": "TestN1", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN2", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN4", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"}
    ]
  },
  {
    name: 'Phase 3',
    revision: 151,
    auditType: "Test Audit",
    ecnId: 876484876956959,
    processStatus: {value: 1},
    dateTimeCreated: '2017-04-12 11:32:54.800',
    phase: {phsNo: 2},
    auditLog: [
      {"Narrative": "TestN1", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN2", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN4", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"}
    ]
  },
  {
    name: 'Phase 4',
    revision: 156,
    auditType: "Test Audit",
    ecnId: 876484876956959,
    processStatus: {value: 1},
    dateTimeCreated: '2017-04-12 11:32:54.800',
    phase: {phsNo: 3},
    auditLog: [
      {"Narrative": "TestN1", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN2", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN4", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"}
    ]
  },
  {
    name: 'Phase 5',
    revision: 149,
    auditType: "Test Audit",
    ecnId: 876484876956959,
    processStatus: {value: 1},
    dateTimeCreated: '2017-04-12 11:32:54.800',
    phase: {phsNo: 4},
    auditLog: [
      {"Narrative": "TestN1", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN2", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN4", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"}
    ]
  },
  {
    name: 'Phase 6',
    revision: 149,
    auditType: "Test Audit",
    ecnId: 876484876956959,
    processStatus: {value: 1},
    dateTimeCreated: '2017-04-12 11:32:54.800',
    phase: {phsNo: 5},
    auditLog: [
      {"Narrative": "TestN1", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN2", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN4", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"},
      {"Narrative": "TestN5", "fieldName": "TestF1", "ErrorDetails": "File Not Found", "ErrorType": "Severe"}
    ]
  }
];
