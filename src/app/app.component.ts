import {Component} from '@angular/core';
import {process, State} from '@progress/kendo-data-query';
import {dummyData} from './products';


var Ajv = require('ajv');
var ajv = new Ajv({allErrors: true}); // options can be passed, e.g. {allErrors: true}

import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'XYZ Company';
  secondTitle = "Dashboard";

  //function for json velidation

  // jvf() {
  //   var validate = ajv.compile('baseObject');
  //   var valid = validate('object for validation');
  //   if (!valid) console.log(validate.errors);
  //   this.validateRes = valid;
  //   this.validateErrorText = JSON.stringify(validate.errors);
  // }

  phaseTypeValue:any = {
    0: 'File Archival',
    1: 'File Verification',
    2: 'Record Validation',
    3: 'Transformation',
    4: 'WCIS_ENRICHMENT',
    5: 'DLEA_INVOKED'
  };
  processStatusValue:any = {
    1: 'Started',
    2: 'In Process',
    3: 'Completed'
  };

  private state:State = {
    skip: 0,
    take: 5
  };

  private gridDataforFilter:GridDataResult = process(dummyData, this.state);

  protected dataStateChange(state:DataStateChangeEvent):void {
    this.state = state;
    this.gridDataforFilter = process(dummyData, this.state);
  }


  public isShowAuditLog(dataItem:any, index:number):boolean {
    return dataItem.auditLog != null && dataItem.auditLog.length > 0;
  }

  public getLegendColor(errorType) {
    var legendclass = "legend ";
    if (errorType == "Severe")
      legendclass += "Red";
    else if (errorType == "Warning")
      legendclass += "Yellow";
    else if (errorType == "Fatal")
      legendclass += "Blue";
    else
      legendclass += "";

    return legendclass;
  }

}
